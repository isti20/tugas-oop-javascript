class Table {
    constructor(init) {
      this.init = init;
    }
  
    createHeader(data) {
      let open = "<thead><tr>";
      let close = "</tr></thead>";
      data.forEach((d) => {
        open += `<th>${d}</th>`;
      });
  
      return open + close;
    }
  
    createBody(data) {
      let open = "<tbody>";
      let close = "</tbody>";
      let count = 0;
  
      data.forEach((d) => {
        count++;
        open += `
          <tr>
            <td>${d[0]}</td>
            <td>${d[1]}</td>
            <td>${d[2]}</td>
            <td>${d[3]}</td>
          </tr>
        `;
      });
  
      return open + close;
    }
  
    render(element) {
      let table =
        "<table class='table table-hover'>" +
        this.createHeader(this.init.columns) +
        this.createBody(this.init.data) +
        "</table>";
      element.innerHTML = table;
    }
  }
  
  const table = new Table({
    columns: ["Number","Name", "Age","Email"],
    data: [
      ["1","Edi Hartono","24", "edi.eduwork@gmail.com"],
      ["2","Dodi Prakoso","25", "dodi@upscale.id"],
      ["3","Budi Setiawan","26", "budi@upscale.id"],
      ["4","Naila Ika","27", "naila@upscale.id"],
      ["5","Jessica Mila","28", "jessica@upscale.id"],
      ["6","Andena","29", "andena@upscale.id"]
    ]
  });
  table.render(container);